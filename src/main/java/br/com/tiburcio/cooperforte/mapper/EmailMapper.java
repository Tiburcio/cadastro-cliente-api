package br.com.tiburcio.cooperforte.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import br.com.tiburcio.cooperforte.dto.EmailDTO;
import br.com.tiburcio.cooperforte.entity.Email;


@Mapper(componentModel = "spring", uses = {})
public interface EmailMapper extends BaseMapper<Email, EmailDTO> {

    @InheritInverseConfiguration(name = "toDto")
    void fromDto(EmailDTO dto, @MappingTarget Email entity);

    default Email fromId(Long id) {
        if (id == null) {
            return null;
        }
        Email email = new Email();
        email.setId(id);
        return email;
    }
}

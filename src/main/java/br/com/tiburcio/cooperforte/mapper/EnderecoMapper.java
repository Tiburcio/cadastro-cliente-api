package br.com.tiburcio.cooperforte.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import br.com.tiburcio.cooperforte.dto.EnderecoDTO;
import br.com.tiburcio.cooperforte.entity.Endereco;

@Mapper(componentModel = "spring", uses = {})
public interface EnderecoMapper extends BaseMapper<Endereco, EnderecoDTO> {

    @InheritInverseConfiguration(name = "toDto")
    void fromDto(EnderecoDTO dto, @MappingTarget Endereco entity);

    default Endereco fromId(Long id) {
        if (id == null) {
            return null;
        }
        Endereco endereco = new Endereco();
        endereco.setId(id);
        return endereco;
    }
}

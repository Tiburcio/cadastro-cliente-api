package br.com.tiburcio.cooperforte.mapper;

import org.mapstruct.Mapper;

import br.com.tiburcio.cooperforte.dto.ClienteDTO;
import br.com.tiburcio.cooperforte.entity.Cliente;

@Mapper(componentModel = "spring", uses = {
        TelefoneMapper.class,
        EmailMapper.class
       // EnderecoMapper.class
})
public interface ClienteMapper extends BaseMapper<Cliente, ClienteDTO> {

    default Cliente fromId(Long id) {
        if (id == null) {
            return null;
        }
        Cliente cliente = new Cliente();
        cliente.setId(id);
        return cliente;
    }
}

package br.com.tiburcio.cooperforte.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import br.com.tiburcio.cooperforte.dto.TelefoneDTO;
import br.com.tiburcio.cooperforte.entity.Telefone;


@Mapper(componentModel = "spring", uses = {})
public interface TelefoneMapper extends BaseMapper<Telefone, TelefoneDTO> {

    @InheritInverseConfiguration(name = "toDto")
    void fromDto(TelefoneDTO dto, @MappingTarget Telefone entity);

    default Telefone fromId(Long id) {
        if (id == null) {
            return null;
        }
        Telefone telefone = new Telefone();
        telefone.setId(id);
        return telefone;
    }
}

package br.com.tiburcio.cooperforte.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.tiburcio.cooperforte.dto.ClienteDTO;
import br.com.tiburcio.cooperforte.entity.Cliente;

import java.util.List;
import java.util.Optional;

public interface ClienteService {

    ClienteDTO save(ClienteDTO clienteDTO);

    Cliente save(Cliente cliente);

    Page<ClienteDTO> findAll(Pageable pageable);

    Optional<ClienteDTO> findOne(Long id);

    Optional<ClienteDTO> findByNuCpf(String nuIdentidade);

    Optional<ClienteDTO> findById(Long id);

    List<ClienteDTO> findAllByNmClienteLike(String nmCliente);

    void delete(Long id);
}

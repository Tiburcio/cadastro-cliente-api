package br.com.tiburcio.cooperforte.service;

import java.util.Optional;

import br.com.tiburcio.cooperforte.dto.EnderecoDTO;
import br.com.tiburcio.cooperforte.exception.BrasilApiException;

public interface CepService {

    Optional<EnderecoDTO> consulta(String nuCep) throws BrasilApiException;
}

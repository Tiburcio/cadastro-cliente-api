package br.com.tiburcio.cooperforte.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.tiburcio.cooperforte.entity.Cliente;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    public Optional<Cliente> findByNuCpf(String id);

    public List<Cliente> findAllByNoClienteLike(String nmCliente);
}

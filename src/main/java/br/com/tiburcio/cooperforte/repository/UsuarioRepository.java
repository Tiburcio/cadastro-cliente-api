package br.com.tiburcio.cooperforte.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.tiburcio.cooperforte.entity.Usuario;

import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long>, JpaSpecificationExecutor<Usuario> {
    Optional<Usuario> findOneByEmailIgnoreCase(String dsEmail);
}
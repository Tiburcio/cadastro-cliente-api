package br.com.tiburcio.cooperforte.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;

import lombok.Data;

@Data
public class ClienteDTO implements Serializable {

    public Long id;

    @NotNull
    @Size(min = 3, max = 100)
    public String noCliente;

    @NotNull
    @CPF
    public String nuCpf;

    @NotNull
    @Valid
    public Set<EmailDTO> emails;

    @NotNull
    @Valid
    public Set<TelefoneDTO> telefones;

    @NotNull
    @Valid
    public EnderecoDTO endereco;

    public Date dtCriado;

    public Date dtModificado;

    public String coUsuarioCadastro;
}

package br.com.tiburcio.cooperforte.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

import br.com.tiburcio.cooperforte.entity.TipoTelefone;

import java.io.Serializable;

@Data
public class TelefoneDTO implements Serializable {

    public Long id;

    @NotNull
    public String nuTelefone;

    @NotNull
    public TipoTelefone coTipoTelefone;
}

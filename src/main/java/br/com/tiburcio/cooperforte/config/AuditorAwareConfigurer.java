package br.com.tiburcio.cooperforte.config;


import org.springframework.data.domain.AuditorAware;

import br.com.tiburcio.cooperforte.security.SecurityUtils;

import java.util.Optional;

public class AuditorAwareConfigurer implements AuditorAware<String> {

    @Override
    public  Optional<String> getCurrentAuditor() {
       return SecurityUtils.getCurrentUserLogin();
    }

}

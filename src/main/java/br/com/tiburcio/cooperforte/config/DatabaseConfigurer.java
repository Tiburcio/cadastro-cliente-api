package br.com.tiburcio.cooperforte.config;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.com.tiburcio.cooperforte.entity.Usuario;
import br.com.tiburcio.cooperforte.repository.UsuarioRepository;

import java.util.Optional;


@Configuration
@EnableJpaRepositories(basePackages = "br.com.tiburcio.cooperforte.repository")
@EnableTransactionManagement
@EnableJpaAuditing(auditorAwareRef="auditorProvider")
@RequiredArgsConstructor
public class DatabaseConfigurer {

    private final Logger log = LoggerFactory.getLogger(DatabaseConfigurer.class);

    // private final UsuarioRepository usuarioRepository;

    @Bean
    public AuditorAware<String> auditorProvider() {
        return new AuditorAwareConfigurer();
    }
}

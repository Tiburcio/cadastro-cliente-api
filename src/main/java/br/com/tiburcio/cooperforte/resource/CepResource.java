package br.com.tiburcio.cooperforte.resource;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import br.com.tiburcio.cooperforte.dto.EnderecoDTO;
import br.com.tiburcio.cooperforte.service.CepService;
import br.com.tiburcio.cooperforte.util.ResponseUtil;


@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class CepResource {

    private final CepService cepService;

    @GetMapping(value = "/cep/{nuCep}")
    public ResponseEntity<EnderecoDTO> consulta(@PathVariable String nuCep){
        return ResponseUtil.wrapOrNotFound(cepService.consulta(nuCep));
    }

}
